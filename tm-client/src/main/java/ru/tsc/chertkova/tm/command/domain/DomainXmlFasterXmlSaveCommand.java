package ru.tsc.chertkova.tm.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.dto.request.data.DataXmlFasterXmlSaveRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.io.FileOutputStream;

public final class DomainXmlFasterXmlSaveCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save date in xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getServiceLocator().getDomainEndpoint().saveDataXmlFasterXml(new DataXmlFasterXmlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
