package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserProfileRequest;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserProfileResponse;
import ru.tsc.chertkova.tm.marker.IntegrationCategory;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getHost();

    @NotNull
    private final String port = propertyService.getPort();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("kakoi-to_user", "1234")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("test", "password")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("test", null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(null, null)));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getToken());
    }

    @Test
    public void profile() {
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        @Nullable String token = response.getToken();
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(new UserProfileRequest("token")));
        UserProfileResponse responseProfile = authEndpoint.profile(new UserProfileRequest(token));
        Assert.assertNotNull(responseProfile);
        @Nullable User user = responseProfile.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("admin", user.getLogin());
    }

}
