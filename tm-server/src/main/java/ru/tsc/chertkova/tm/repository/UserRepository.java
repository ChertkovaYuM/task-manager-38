package ru.tsc.chertkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_USER";

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull ResultSet row) {
        @NotNull final User project = new User();
        project.setId(row.getString("ID"));
        project.setCreated(row.getTimestamp("CREATED"));
        project.setLogin(row.getString("LOGIN"));
        project.setFirstName(row.getString("FIRST_NAME"));
        project.setMiddleName(row.getString("MIDDLE_NAME"));
        project.setLastName(row.getString("LAST_NAME"));
        project.setEmail(row.getString("EMAIL"));
        project.setPasswordHash(row.getString("PASSWORD"));
        project.setRole(Role.valueOf(row.getString("ROLE")));
        project.setLocked(row.getBoolean("LOCKED"));
        return project;
    }

    @Override
    @SneakyThrows
    public User add(@NotNull User model) {
        @NotNull final User user = new User(model.getLogin(),
                model.getPasswordHash(), model.getEmail(), model.getFirstName(),
                model.getLastName(), model.getMiddleName());
        @NotNull final String sql = "INSERT INTO " + "public.\"" + getTableName() +
                "\" (\"ID\", \"CREATED\", \"LOGIN\", \"FIRST_NAME\", \"MIDDLE_NAME\", \"LAST_NAME\", \"EMAIL\", \"PASSWORD\", \"ROLE\", \"LOCKED\") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setTimestamp(2, new Timestamp(user.getCreated().getTime()));
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getFirstName());
        statement.setString(5, user.getMiddleName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getEmail());
        statement.setString(8, user.getPasswordHash());
        statement.setString(9, user.getRole().toString());
        statement.setBoolean(10, user.getLocked());
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User update(@Nullable User user) {
        @NotNull final String sqlCommand = String.format(
                "UPDATE %s SET %s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ?,%s = ? WHERE ID = ?"
                , "public.\"" + getTableName() + "\"",
                "LOGIN", "PASSWORD", "EMAIL", "FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "ROLE", "LOCKED", "CREATED", "ID"
        );
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPasswordHash());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getFirstName());
            preparedStatement.setString(5, user.getMiddleName());
            preparedStatement.setString(6, user.getLastName());
            preparedStatement.setString(7, user.getRole().getDisplayName());
            preparedStatement.setBoolean(8, user.getLocked());
            preparedStatement.setString(9, user.getId());
            preparedStatement.executeUpdate();
            return user;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"LOGIN\" = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        @NotNull final String sql = "SELECT * FROM public.\"" + getTableName() + " WHERE EMAIL = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        @NotNull final String sql = "DELETE FROM public.\"" + getTableName() + "\" WHERE \"LOGIN\" = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sqlCommand = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"LOGIN\" = ?";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, login);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sqlCommand = "SELECT * FROM public.\"" + getTableName() + "\" WHERE \"EMAIL\" = ?";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, email);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
    }

}
