package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnerService<Project> {

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    Project create(@Nullable String userId, @Nullable String name,
                   @Nullable String description, @Nullable Date dateBegin,
                   @Nullable Date dateEnd);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
