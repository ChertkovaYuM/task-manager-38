package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M>, IService<M> {

}
