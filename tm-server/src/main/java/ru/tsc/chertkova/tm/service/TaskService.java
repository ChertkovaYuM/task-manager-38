package ru.tsc.chertkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnerService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    protected @NotNull ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task create(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final Date dateBegin,
                       @Nullable final Date dateEnd) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.create(userId, name, description);
            result.setDateBegin(dateBegin);
            result.setDateEnd(dateEnd);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @Nullable List<Task> result;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findAllByProjectId(userId, projectId);
            if (result.isEmpty()) throw new TaskNotFoundException();
            return result;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId, @Nullable final String id,
                           @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setName(name);
            result.setDescription(description);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findById(id) != null;
    }

}
