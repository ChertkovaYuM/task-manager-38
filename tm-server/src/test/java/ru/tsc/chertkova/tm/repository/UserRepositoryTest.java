package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.service.ConnectionService;
import ru.tsc.chertkova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserRepository repository = new UserRepository(connectionService.getConnection());

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findAll().get(0));
    }

    @Test
    public void addAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
    }

    private void addAll(IUserRepository repository, List<User> users) {
        for (User u :
                users) {
            repository.add(u);
        }
    }

    @Test
    public void remove() {
        @NotNull final List<User> list = new ArrayList<>(USER_LIST);
        addAll(repository, USER_LIST);
        repository.remove(USER2);
        list.remove(USER2);
        Assert.assertEquals(list, repository.findAll());
    }

    @Test
    public void clear() {
        addAll(repository, USER_LIST);
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN1);
        repository.add(USER2);
        repository.add(USER1);
        Assert.assertEquals(
                Arrays.asList(ADMIN1, USER2, USER1),
                repository.findAll()
        );
    }

    @Test
    public void findById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, USER_LIST);
        Assert.assertEquals(
                repository.findById(USER2.getId()),
                USER2
        );
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, USER_LIST);
        Assert.assertEquals(
                repository.findByLogin(USER2.getLogin()),
                USER2
        );
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, USER_LIST);
        Assert.assertEquals(
                repository.removeById(USER2.getId()),
                USER2
        );
        @NotNull final List<User> list = new ArrayList<>(USER_LIST);
        list.remove(USER2);
        Assert.assertEquals(repository.findAll(), list);
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(repository.findAll().isEmpty());
        addAll(repository, USER_LIST);
        repository.removeByLogin(USER2.getLogin());
        @NotNull final List<User> list = new ArrayList<>(USER_LIST);
        list.remove(USER2);
        Assert.assertEquals(repository.findAll(), list);
    }

}
