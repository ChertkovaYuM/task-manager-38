package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static ru.tsc.chertkova.tm.constant.ProjectTestData.*;
import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connectionService.getConnection());

    @NotNull
    private final IProjectService service = new ProjectService(connectionService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        for (Project p :
                PROJECT_LIST) {
            repository.add(p);
        }
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(PROJECT_LIST, service.findAll());
    }

    @Test
    public void FindAllByUserId() {
        Assert.assertEquals(ADMIN1_PROJECT_LIST, service.findAll(ADMIN1.getId()));
        thrown.expect(UserIdEmptyException.class);
        service.findAll();
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        service.clear(USER1.getId());
        Assert.assertTrue(repository.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        service.clear(null);
    }

    @Test
    public void remove() {
        @NotNull final List<Project> list = new ArrayList<>(repository.findAll());
        //@Nullable final Project removed = service.remove(USER1_PROJECT2);
        //Assert.assertEquals(USER1_PROJECT2, removed);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, repository.findAll());
        //Assert.assertNull(service.remove(null));
        //Assert.assertNull(service.remove(USER1_PROJECT2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<Project> list = new ArrayList<>(repository.findAll());
        service.remove(USER1.getId(), USER1_PROJECT2);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, repository.findAll());
        service.remove(USER1.getId(), null);
        Assert.assertEquals(list, repository.findAll());
        service.remove(USER1.getId(), USER2_PROJECT1);
        Assert.assertEquals(list, repository.findAll());
        thrown.expect(UserIdEmptyException.class);
        service.remove(null, USER2_PROJECT1);
        Assert.assertEquals(list, repository.findAll());
    }

    @Test
    public void createByNameAndUserId() {
        repository.clear();
        @NotNull final Project expected = new Project();
        expected.setName(USER1_PROJECT1.getName());
        expected.setUserId(USER1.getId());
        //service.create(USER1.getId(), USER1_PROJECT1.getName());
        @NotNull final Project created = repository.findAll().get(0);
        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
        thrown.expect(UserIdEmptyException.class);
        //service.create(null, USER1_PROJECT1.getName());
        thrown.expect(NameEmptyException.class);
        //service.create(USER1.getId(), null);
    }

    @Test
    public void createByNameAndUserIdAndDescription() {
        repository.clear();
        @NotNull final Project expected = new Project();
        expected.setName(USER1_PROJECT1.getName());
        expected.setUserId(USER1.getId());
        expected.setDescription(USER1_PROJECT1.getDescription());
        service.create(USER1.getId(), USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());
        @NotNull final Project created = repository.findAll().get(0);
        assertThat(created).isEqualToIgnoringGivenFields(expected, "id");
        thrown.expect(UserIdEmptyException.class);
        service.create(null, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());
        thrown.expect(NameEmptyException.class);
        service.create(USER1.getId(), null, USER1_PROJECT1.getDescription());
        thrown.expect(DescriptionEmptyException.class);
        service.create(USER1.getId(), USER1_PROJECT1.getName(), null);
    }

    @Test
    public void addByUserId() {
        repository.clear();
        service.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
        service.add(USER1.getId(), null);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
        thrown.expect(UserIdEmptyException.class);
        service.add(null, USER1_PROJECT1);
    }

    @Test
    public void findById() {
        Assert.assertEquals(
                service.findById(USER1.getId(), USER1_PROJECT2.getId()),
                USER1_PROJECT2
        );
        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_PROJECT2.getId()));
        thrown.expect(UserIdEmptyException.class);
        service.findById(null, USER1_PROJECT1.getId());
        thrown.expect(IdEmptyException.class);
        service.findById(USER1.getId(), null);
    }

    @Test
    public void removeById() {
        @NotNull final List<Project> expected = new ArrayList<>(repository.findAll());
        Assert.assertEquals(
                service.removeById(USER1.getId(), USER1_PROJECT2.getId()),
                USER1_PROJECT2
        );
        expected.remove(USER1_PROJECT2);
        Assert.assertEquals(expected, repository.findAll());
        thrown.expect(UserIdEmptyException.class);
        service.removeById(null, USER1_PROJECT1.getId());
        thrown.expect(IdEmptyException.class);
        service.removeById(USER1.getId(), null);
    }

    @Test
    public void updateById() {
        @NotNull final Project actual = new Project();
        actual.setUserId(ADMIN1.getId());
        repository.add(actual);
        service.updateById(
                ADMIN1.getId(),
                actual.getId(),
                ADMIN1_PROJECT1.getName(),
                ADMIN1_PROJECT1.getDescription()
        );
        assertThat(actual).isEqualToIgnoringGivenFields(ADMIN1_PROJECT1, "id");
        thrown.expect(UserIdEmptyException.class);
        service.updateById(null,
                actual.getId(),
                ADMIN1_PROJECT1.getName(),
                ADMIN1_PROJECT1.getDescription()
        );
        thrown.expect(IdEmptyException.class);
        service.updateById(ADMIN1.getId(),
                null,
                ADMIN1_PROJECT1.getName(),
                ADMIN1_PROJECT1.getDescription()
        );
        thrown.expect(NameEmptyException.class);
        service.updateById(ADMIN1.getId(),
                actual.getId(),
                null,
                ADMIN1_PROJECT1.getDescription()
        );
    }

}
