package ru.tsc.chertkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

public final class DataXmlFasterXmlLoadRequest extends AbstractUserRequest {

    public DataXmlFasterXmlLoadRequest(@Nullable String token) {
        super(token);
    }

}
