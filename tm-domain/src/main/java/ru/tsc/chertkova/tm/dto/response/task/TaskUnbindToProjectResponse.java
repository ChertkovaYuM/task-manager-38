package ru.tsc.chertkova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Task;

@NoArgsConstructor
public class TaskUnbindToProjectResponse extends AbstractTaskResponse {

    public TaskUnbindToProjectResponse(@Nullable Task task) {
        super(task);
    }

}
