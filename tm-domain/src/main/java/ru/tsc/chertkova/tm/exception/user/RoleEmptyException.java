package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.field.AbstractFieldException;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
